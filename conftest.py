import pytest

def pytest_collection_finish(session):
    # pytestのhook。ここにpyannotateを差し込む。
    from pyannotate_runtime import collect_types
    collect_types.init_types_collection()

@pytest.fixture(autouse=True)
def collect_types_fixture():
    from pyannotate_runtime import collect_types
    collect_types.start()
    yield
    collect_types.stop()

def pytest_sessionfinish(session, exitstatus):
    # type_info.jsonに型情報をまとめる。
    from pyannotate_runtime import collect_types
    collect_types.dump_stats("type_info.json")