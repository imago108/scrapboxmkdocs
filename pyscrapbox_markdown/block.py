from enum import IntEnum, auto
import re

from .utility import convert_link, convert_emphasis


class EScrapBoxBlockType(IntEnum):
    INVALID = -1
    PLAIN_TEXT = 0
    TITLE = auto()
    HEADER = auto()
    BULLET = auto()
    QUOTE = auto()
    CODE = auto()
    EXTRA = auto()
    ADMONITION = auto()


class ScrapBoxBlockBase:
    def __init__(self) -> None:
        self.type = EScrapBoxBlockType.INVALID
        self.text_list = []

    def __repr__(self):
        return f"<ScraoBoxBlock:{str(self.type)}>"

    def get_type(self) -> EScrapBoxBlockType:
        return self.type

    def commit_text(self, in_text: str) -> None:
        self.text_list.append(in_text)

    def markdown(self):
        raise NotImplementedError()


class ScrapBoxBlockPlainText(ScrapBoxBlockBase):
    def __init__(self) -> None:
        super().__init__()
        self.type = EScrapBoxBlockType.PLAIN_TEXT

    def markdown(self) -> str:
        end_index = len(self.text_list) - 1
        result = ""
        for i, text in enumerate(self.text_list):
            # convert scrapbox emphasis to markdown
            text = convert_emphasis(text)
            # convert scrapbox link to markdown
            text = convert_link(text)
            if i == end_index:
                result += f"{text}"
            else:
                result += f"{text}  \n"
        return result


class ScrapBoxBlockTitle(ScrapBoxBlockBase):
    def __init__(self) -> None:
        self.type = EScrapBoxBlockType.TITLE
        self.text = ""
        self.category_branch = []

    def commit_text(self, in_text: str) -> None:
        # get category
        splitter = "::"
        if splitter in in_text:
            self.category_branch = in_text.split(splitter)[:-1]
            self.text = in_text.split(splitter)[-1]
        else:
            self.text = in_text

    def markdown(self) -> str:
        return f"# {self.text}"


class ScrapBoxBlockBullet(ScrapBoxBlockBase):
    def __init__(self) -> None:
        super().__init__()
        self.type = EScrapBoxBlockType.BULLET

    def markdown(self) -> str:
        # はじめに, 先頭にある空白文字, タブ文字の個数を数える
        pattern = r"^(\s+)(.+)"
        result = ""
        for i, text in enumerate(self.text_list):
            text = convert_link(text)

            ma = re.match(pattern, text)
            if not ma:
                continue

            indent_depth = len(ma.groups()[0]) - 1

            tmp_text = ""
            # インデントの深さを, スペース4に変換する
            for _ in range(indent_depth):
                tmp_text += "    "
            tmp_text += f"* {ma.groups()[1]}"

            result += f"{tmp_text}"
            if i < len(self.text_list) - 1:
                result += "\n"

        return result


class ScrapBoxBlockHeader(ScrapBoxBlockBase):
    def __init__(self) -> None:
        self.type = EScrapBoxBlockType.HEADER
        self.text = ""
        self.strength = 0
        self.maxStrength = 0

    def commit_text(self, in_text: str) -> None:
        self.text = in_text

    def markdown(self) -> str:
        result = "##"
        weak = self.maxStrength - self.strength
        for _ in range(weak):
            result += "#"
        return f"{result} {self.text}"


class ScrapBoxBlockCode(ScrapBoxBlockBase):
    def __init__(self) -> None:
        self.type = EScrapBoxBlockType.CODE
        self.text_list = []
        self.title = ""

    def commit_text(self, in_text: str) -> None:
        # 最初にコミットされたものは, タイトルである
        if "code:" in in_text:
            self.title = in_text.replace("code:", "")
        else:
            temp_text = in_text
            # 先頭のスペース あるいは タブ文字を削除する
            pattern = r"^\s"
            temp_text = re.sub(pattern, "", temp_text)
            # 次に, 先頭のタブを スペース4に置換する
            pattern = r"^(\t+)"
            if ma := re.match(pattern, temp_text):
                tab_count = len(ma.groups()[0])
                target_space = ""
                for _ in range(tab_count):
                    target_space += "    "
                temp_text = re.sub(pattern, target_space, temp_text)
            self.text_list.append(temp_text)

    def markdown(self) -> str:
        cap = "```"
        result = f"{cap}{self.title}\n"
        for text in self.text_list:
            result += f"{text}\n"
        result += cap
        return result


class ScrapBoxBlockAdmonitions(ScrapBoxBlockCode):
    """
    Admonitions Block
    https://squidfunk.github.io/mkdocs-material/reference/admonitions/

    """

    def __init__(self) -> None:
        self.type = EScrapBoxBlockType.ADMONITION
        self.text_list = []
        self.title = ""

    def markdown(self) -> str:
        cap = "!!! "
        result = f"{cap}{self.title}\n"
        for text in self.text_list:
            result += f"    {text}\n"
        return result


class ExtraBlock:
    def __init__(self) -> None:
        self.text_list = []

    def commit_text(self, in_text: str) -> None:
        self.text_list.append(in_text)

    def get_type(self) -> EScrapBoxBlockType:
        return EScrapBoxBlockType.EXTRA

    def markdown(self) -> str:
        result = ""
        end_index = len(self.text_list) - 1
        for i, text in enumerate(self.text_list):
            if i == end_index:
                result += text
            else:
                result += f"{text}\n"

        return result

