from dataclasses import dataclass, field
from typing import List
import re
from datetime import timedelta, timezone, datetime

from pyscrapbox.page import ScrapBoxPage

from .block import *

from logging import getLogger, Formatter, StreamHandler, DEBUG

logger = getLogger(__name__)
fmt = Formatter("%(asctime)s - %(filename)s::%(funcName)s[%(levelname)s] %(message)s")
sh = StreamHandler()
sh.setLevel(DEBUG)
sh.setFormatter(fmt)
logger.setLevel(DEBUG)
logger.addHandler(sh)



@dataclass
class MkDocsPage:
    id: str
    title: str
    categoryBranch: List[str]
    text_list: List[str] = field(default_factory=list, init=False)
    created: datetime = field(init=False)
    updated: datetime = field(init=False)
    generated: datetime = field(init=False)
    url: str = field(init=False)

    @classmethod
    def from_scrapbox(cls, scrapbox_page: ScrapBoxPage, pattern_publish: str = "") -> "MkDocsPage":
        if not scrapbox_page:
            return None

        id = scrapbox_page.id
        if "::" in scrapbox_page.title:
            t = scrapbox_page.title.split("::")
            title = t[-1]
            categoryBranch = t[:-1]
        else:
            title = scrapbox_page.title
            categoryBranch = []

        mk_page = MkDocsPage(
            id=id,
            title=title,
            categoryBranch=categoryBranch,
        )

        mk_page.text = mk_page.convert_scrapbox_to_markdown_v1(
            scrapbox_page, pattern_publish=pattern_publish
        )

        mk_page.created = scrapbox_page.created
        mk_page.updated = scrapbox_page.updated
        mk_page.generated = datetime.now()

        return mk_page

    def convert_scrapbox_to_markdown_v1(self, scrapbox_page: ScrapBoxPage, pattern_publish: str = "") -> str:

        raw_text = scrapbox_page.text

        # ScrapBoxのテキストをBlockに分割する
        active_block = None
        block_list = []
        for line in raw_text.splitlines():
            # 先頭は必ずTitleとする
            if not block_list:

                # Titleの挿入
                title = ScrapBoxBlockTitle()
                title.commit_text(line)

                # カテゴリが存在していれば, カテゴリのパンくずリストを作成する
                # Option: 先頭にカテゴリのパンくずリストを挿入する
                # ISSUE: Optionにする
                if len(title.category_branch) > 0:
                    block_category_tree = ExtraBlock()
                    block_category_tree.commit_text('<div class="kr-article-category-tree">')
                    for category in title.category_branch:
                        block_category_tree.commit_text('<span>%s</span>' % category)
                    block_category_tree.commit_text('</div>')
                    block_list.append(block_category_tree)

                # Title Blockを挿入
                block_list.append(title)

                # Option: Scrapboxの記事作成日と更新日を挿入する
                # ISSUE: Optionにする, 挿入位置も
                block_date = ExtraBlock()
                block_date.commit_text('<div class="kr-date">')
                # ISSUE: TimeZoneを設定可能にする
                TZ = timezone(timedelta(hours=+9), "JST")
                # 記事作成日の作成
                page_created = scrapbox_page.created
                block_date.commit_text('<span class="kr-created">')
                block_date.commit_text(page_created.astimezone(TZ).strftime("%Y/%m/%d"))
                block_date.commit_text("</span>")
                # 一日以上はなれていれば, 記事更新日を作成する
                page_updated = scrapbox_page.updated
                one_day = timedelta(days=+1)
                if page_updated - page_created > one_day:
                    block_date.commit_text('<span class="kr-updated">')
                    block_date.commit_text(
                        page_updated.astimezone(TZ).strftime("%Y/%m/%d")
                    )
                    block_date.commit_text("</span>")

                block_date.commit_text("</div>")

                block_list.append(block_date)
                continue

            # ActiveなBlockがない, かつ空行ならスキップする
            if not active_block and not line:
                continue

            # ActiveなBlockがあり, 空行が見つかったら, ブロックを確定させる
            if active_block and not line:
                block_list.append(active_block)
                active_block = None
                continue

            # Publish Pattterにマッチする部分は予め取り除いておく
            if pattern_publish:
                line = re.sub(pattern_publish, "", line, re.IGNORECASE)

            # ====================
            # Admonitions block
            # ====================
            # code:note が見つかったら, 前のスタックを確定させて, AdmonitionsBlockを開始する
            if "code:note" in line or "code:tldr" in line or "code:tip" in line or "code:example" in line or "code:quote" in line:
                if active_block:
                    block_list.append(active_block)
                    active_block = None
                active_block = ScrapBoxBlockAdmonitions()
                active_block.commit_text(line)
                continue

            # 現在ActiveなブロックがAdmonitionsBlockならば, テキストをコミットする
            if active_block and active_block.type == EScrapBoxBlockType.ADMONITION:
                active_block.commit_text(line)
                continue

            # ====================
            # CodeBlock
            # ====================
            # code: が見つかったら, 前のスタックを確定させて, CodeBlockを開始する
            if "code:" in line:
                if active_block:
                    block_list.append(active_block)
                    active_block = None
                active_block = ScrapBoxBlockCode()
                active_block.commit_text(line)
                continue

            # 現在ActiveなブロックがCodeBlockならば, テキストをコミットする
            if active_block and active_block.type == EScrapBoxBlockType.CODE:
                active_block.commit_text(line)
                continue

            # ====================
            # HeaderBlock
            # ====================
            # Headerの正規表現に一致する部分が見つかったら, HeaderBlockを確定する
            # pattern = r"^[^\S\n\r]*\[(\*+)\s+(.+)\]\s*$"
            pattern = r"^[^\S\n\r]*\[(\*+)\s+([^\[]+?)\s*\]\s*$"
            if m := re.match(pattern, line):
                # 前のスタックを確定させる
                if active_block:
                    block_list.append(active_block)
                    active_block = None
                # get matching object
                header_strength = len(m.groups()[0])
                header_title = m.groups()[1]
                # make header object
                header = ScrapBoxBlockHeader()
                header.strength = header_strength
                header.commit_text(header_title)
                block_list.append(header)
                continue

            # ===================
            # PlainText / Bullet の確定
            # ===================
            # 先頭行の空白/タブ文字の数を見る
            pattern = r"^(\s+)"
            if m := re.match(pattern, line):
                if active_block:
                    # ActiveなBlockがあり, BulletBlockの場合, 引き続きコミットを行う
                    if active_block.get_type() == EScrapBoxBlockType.BULLET:
                        active_block.commit_text(line)
                        continue
                    # ActiveなBlockがあるが,BulletBlockでないばあい, 前のブロックを確定する
                    else:
                        block_list.append(active_block)
                        active_block = None

                if not active_block:
                    # ActiveなBlockがない場合, BulletBlockを作成する
                    active_block = ScrapBoxBlockBullet()
                    active_block.commit_text(line)
                    continue

            else:
                # 空白/タブ文字が存在していないばあい, PlainTextBlockにいる
                if active_block:
                    # ActiveなBlockがあり, PlainTextBlockの場合, 引き続きコミットを行う
                    if active_block.get_type() == EScrapBoxBlockType.PLAIN_TEXT:
                        active_block.commit_text(line)
                        continue
                    # ActiveなBlockがあるが, PlainTextBlockでないばあい, 前のブロックを確定する
                    else:
                        block_list.append(active_block)
                        active_block = None

                # ActiveBlockがない場合, PlainTextBlockを作成する
                if not active_block:
                    active_block = ScrapBoxBlockPlainText()
                    active_block.commit_text(line)
                    continue

        else:
            if active_block:
                block_list.append(active_block)
                active_block = None

        # 成形用に, 一番強いヘッダーの強さを取得する
        max_header_strength = 0
        for block in block_list:
            if block.get_type() == EScrapBoxBlockType.HEADER:
                strength = block.strength
                max_header_strength = max(max_header_strength, strength)
        for block in block_list:
            if block.get_type() == EScrapBoxBlockType.HEADER:
                block.maxStrength = max_header_strength

        result = ""
        for block_i, block in enumerate(block_list):
            # print("--------------------------------")
            # print(block)
            # print(block.text_list)

            result += block.markdown()
            result += "\n\n"

        return result
