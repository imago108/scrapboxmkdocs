def generate_nav(article_list):
    """
    generate nav map
    """

    result = []
    result.append('nav:')
    result.append('  - Home: "index.md"')

    category_map = {}

    for mk_page in article_list:
        categoryBranch = mk_page.categoryBranch
        # hash
        hash_cb = ""
        for category in categoryBranch:
            hash_cb += "/%s" % category

        if hash_cb not in category_map:
            category_map[hash_cb] = []
        category_map[hash_cb].append(mk_page)

    category_map = sorted(category_map.items(), key=lambda x:x[0])
    created_category = []

    for hash_cb, mk_page_list in category_map:
        # check category
        category_list = hash_cb.split("/")[1:]
        indent = 0
        if len(category_list) > 0:
            indent = 1
            for nest, category in enumerate(category_list):
                # gen hash
                ha = ""
                for t in category_list[:nest+1]:
                    ha += "/%s" % t
                if ha not in created_category:
                    text = ""
                    for i in range(indent):
                        text += "  "
                    text += "- %s:" % category
                    result.append(text)
                    created_category.append(ha)
                indent += 1
        else:
            indent = 1

        for mk_page in mk_page_list:
            text = ""
            for i in range(indent):
                text += "  "
            text += '- %s: "%s.md"' % (mk_page.title, mk_page.url)
            result.append(text)

    return result
