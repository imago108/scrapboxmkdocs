import re

# create logger
from logging import getLogger, Formatter, StreamHandler, DEBUG

logger = getLogger(__name__)
fmt = Formatter("%(asctime)s - %(filename)s::%(funcName)s[%(levelname)s] %(message)s")
sh = StreamHandler()
sh.setLevel(DEBUG)
sh.setFormatter(fmt)
logger.setLevel(DEBUG)
logger.addHandler(sh)


def convert_link(text: str) -> str:
    """
    Convert scrapbox link to markdown link
    """

    # 1. Convert URL Type-A
    # [Foo Bar $SOME_URL] => [Foo Bar]($SOME_URL)
    pattern = r"\[\s*(.+?)\s+(https?://\S+)\s*\]"
    prog = re.compile(pattern)
    for match in prog.finditer(text):
        orig_text = match.group()
        title = match.groups()[0]
        url = match.groups()[1]
        text = text.replace(orig_text, f"[{title}]({url})" + '{: class="external-link" }')
        logger.debug(f"Convert URL: {orig_text} => [{title}]({url})")

    # 2. Convert URL Type-B
    # [$SOME_URL Foo Bar] => [Foo Bar]($SOME_URL)
    pattern = r"\[\s*(https?://\S+)\s+(.+?)\s*\]"
    prog = re.compile(pattern)
    for match in prog.finditer(text):
        orig_text = match.group()
        title = match.groups()[1]
        url = match.groups()[0]
        text = text.replace(orig_text, f"[{title}]({url})" + '{: class="external-link" }')
        logger.debug(f"Convert URL: {orig_text} => [{title}]({url})")

    # Convert URL Type-C
    # [$SOME_URL] => $SOME_URL
    image_flags = (".png", ".jpg", ".jpeg", ".gif", ".webp")
    pattern = r"\[\s*(https?://\S+?)\s*\]"
    cond = re.compile(pattern)
    for ma in cond.finditer(text):
        orig_text = ma.group()
        url = ma.groups()[0]
        is_image_url = False
        for i_f in image_flags:
            if i_f in url:
                is_image_url = True
                break
        if is_image_url:
            text = text.replace(orig_text, f"![]({url})")
            logger.debug(f"Convert URL: {orig_text} => ![]({url})")
        else:
            text = text.replace(orig_text, f"[{url}]({url})")
            logger.debug(f"Convert URL: {orig_text} => [{url}]({url})")

    # Convert Internal URL
    # [Internal Link] -> Internal Link
    pattern = "\\[\\s*(.+?)\\s*\\](?![\\(])"
    cond = re.compile(pattern)
    for match in cond.finditer(text):
        orig_text = match.group()
        title = match.groups()[0]
        if ".icon" in title:
            text = text.replace(orig_text, "")
            logger.debug(f"Founded .icon, Hide text: {orig_text}")
        else:
            text = text.replace(orig_text, f"{title}")
            logger.debug(f"Convert Internal Link: {orig_text} => {title}")

    return text


def convert_emphasis(text: str) -> str:
    # Convert Emphasis::Bold Type-A
    # [* Foo Bar] => **Foo Bar**
    pattern = r"\[\*+\s+(.*)\s*\]"
    cond = re.compile(pattern)
    for match in cond.finditer(text):
        orig_text = match.group()
        title = match.groups()[0]
        text = text.replace(orig_text, f"**{title}**")
        logger.debug(f"Convert Emphasis: {orig_text} => **{title}**")

    # Convert Emphasis::Bold Type-B
    # [[Foo Bar]] => **Foo Bar**
    pattern = r"\[\[\s*(.*?)\s*\]\]"
    cond = re.compile(pattern)
    for match in cond.finditer(text):
        orig_text = match.group()
        title = match.groups()[0]
        text = text.replace(orig_text, f"**{title}**")
        logger.debug(f"Convert Emphasis: {orig_text} => **{title}**")

    return text
