from pyscrapbox_markdown.markdown import MkDocsPage
from pyscrapbox.page import ScrapBoxPage


def test_sb_to_md():

    # read sample files
    filepath = "tests/samples/scrapbox_01.txt"
    with open(filepath, mode="r") as f:
        text = f.read()

    sbpage = ScrapBoxPage.from_json({}, "foo")
    sbpage.text = text
    mkpage = MkDocsPage.from_scrapbox(sbpage)

    with open("docs/foo.md", mode="w") as f:
        f.write(mkpage.text)
