import os
import pytest
from dotenv import load_dotenv

from pyscrapbox import ScrapBoxProject


class _settings:
    def __init__(self):
        self.project = ""
        self.secret = ""


@pytest.fixture
def settings():
    # load from dotenv
    filepath_env = os.path.join(os.path.dirname(__file__), "../.env")
    load_dotenv(filepath_env)
    SCRAPBOX_PROJECT = os.getenv("SCRAPBOX_PROJECT")
    SCRAPBOX_SECRET = os.getenv("SCRAPBOX_SECRET")

    stg = _settings()
    stg.project = SCRAPBOX_PROJECT
    stg.secret = SCRAPBOX_SECRET
    return stg


def test_project_create(settings):
    # check access to public scrapbox
    project = ScrapBoxProject.fetch_project("help")
    assert project is not None
    assert project.name == "help"

    # check access to private scrapbox
    project = ScrapBoxProject.fetch_project(settings.project, settings.secret)
    assert project is not None
    assert project.name == settings.project

    # invalid sig
    project = ScrapBoxProject.fetch_project(settings.project, "INVALID_SIG")
    print(project)
    assert project is None


def test_fetch_all_pages(settings):
    # check access to public scrapbox pages
    project = ScrapBoxProject.fetch_project("help")
    project.fetch_pages()

    # check access to private scrapbox pages
    project = ScrapBoxProject.fetch_project(settings.project, settings.secret)
    project.fetch_pages(settings.secret)
