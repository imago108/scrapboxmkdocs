from pyscrapbox_markdown.utility import convert_link, convert_emphasis


def test_convert_link():
    # external link
    text = "[foo https://foo.com]"
    result = convert_link(text)
    assert result == "[foo](https://foo.com)"

    # external link
    text = "[https://foo.com foo]"
    result = convert_link(text)
    assert result == "[foo](https://foo.com)"

    # external link
    text = "[foo http://bar.org] bar [https://baz.com foo]"
    result = convert_link(text)
    assert result == "[foo](http://bar.org) bar [foo](https://baz.com)"

    # external link
    text = "[http://bar.org]"
    result = convert_link(text)
    assert result == "[http://bar.org](http://bar.org)"

    # image link
    text = "[https://foo/bar.png]"
    result = convert_link(text)
    assert result == "![](https://foo/bar.png)"

    # internal link
    text = "[foo bar]"
    result = convert_link(text)
    assert result == "foo bar"


def test_convert_emphasis():
    # bold type-a
    text = "[* foo]"
    result = convert_emphasis(text)
    assert result == "**foo**"

    # bold type-b
    text = "[[foo bar]]"
    result = convert_emphasis(text)
    assert result == "**foo bar**"
