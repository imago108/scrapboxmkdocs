# scrapbox-mkdocs

ScrapBoxの記事をMkDocsで公開する

## Deploy (GitLab CI/CD) 設定
- `gitlab-pages`にDeployを行うための設定

### Variablesの設定
- `Settings > CI / CD > Variables` にて以下のVariablesを設定
- `SCRAPBOX_PROJECT`: `Scrapboxのプロジェクト名`
    - `https://scrapbox.io/help/` の場合, `help`
- `SCRAPBOX_SECRET`: `sid`
    - Private設定のScrapboxの場合, SIDを設定する
    - [ScrapboxのprivateプロジェクトのAPIを叩く](https://scrapbox.io/nishio/Scrapbox%E3%81%AEprivate%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%81%AEAPI%E3%82%92%E5%8F%A9%E3%81%8F)

### Pipelineの実行
- Pipelineを実行して, `gitlab-pages`にDeployを行う

## ローカル環境での実行
### 環境変数の設定
- `.env.sample`を`.env`という名前で複製して, Scrapboxの設定を記述する

### markdownの生成
- `$ python build_markdown_from_scrapbox.py`

### mkdocsの実行
- `$ mkdocs serve`