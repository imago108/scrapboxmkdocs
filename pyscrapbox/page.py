from dataclasses import dataclass, field
from typing import List, Dict
from datetime import datetime, timezone

from . import api


@dataclass
class ScrapBoxPage:
    """
    Class for ScrapBox page
    """

    id: str
    title: str
    image: str
    descriptions: List[str]
    pin: int
    created: datetime
    updated: datetime
    projectName: str
    text: str = field(default="", init=False)

    @classmethod
    def from_json(cls, json: Dict, project_name: str) -> "ScrapBoxPage":
        return ScrapBoxPage(
            id=json.get("id", ""),
            title=json.get("title", ""),
            image=json.get("image", ""),
            descriptions=json.get("descriptions", ""),
            pin=json.get("pin", ""),
            created=datetime.fromtimestamp(json.get("created", 0), timezone.utc),
            updated=datetime.fromtimestamp(json.get("updated", 0), timezone.utc),
            projectName=project_name,
        )

    def is_pinned(self):
        return True if self.pin > 0 else False

    def fetch_text(self, sid=""):
        """
        Fetch page text from API
        """
        project_name = self.projectName
        page_title = self.title

        if res := api.get_page_text(project_name, page_title, sid):
            self.text = res.text
            return self.text
        else:
            return ""
