from dataclasses import dataclass, field
from typing import List
from datetime import datetime, timezone

from .page import ScrapBoxPage
from . import api

from logging import getLogger, Formatter, StreamHandler, DEBUG
from typing import Any
from typing import Dict
from typing import Optional

logger = getLogger(__name__)
fmt = Formatter("%(asctime)s - %(filename)s::%(funcName)s[%(levelname)s] %(message)s")
sh = StreamHandler()
sh.setLevel(DEBUG)
sh.setFormatter(fmt)
logger.setLevel(DEBUG)
logger.addHandler(sh)


@dataclass
class ScrapBoxProject:
    """
    Class for ScrapBox project
    """

    # project info
    id: str
    name: str
    displayName: str
    publicVisible: bool
    created: datetime
    updated: datetime
    pageCount: int = field(default=-1, init=False)

    # request options
    sid: str

    # page instances
    pages: List[ScrapBoxPage] = field(default_factory=list, init=False)

    @classmethod
    def from_json(cls, json: Dict[str, Any], sid: str = "") -> "ScrapBoxProject":
        return ScrapBoxProject(
            id=json.get("id", ""),
            name=json.get("name", ""),
            displayName=json.get("displayName", ""),
            publicVisible=json.get("publicVisible", True),
            created=datetime.fromtimestamp(json.get("created", 0), timezone.utc),
            updated=datetime.fromtimestamp(json.get("updated", 0), timezone.utc),
            sid=sid,
        )

    @classmethod
    def fetch_project(cls, project_name: str, sid: str = "") -> Optional["ScrapBoxProject"]:
        """
        Fetch ScrapBox project info from API
        """
        if res := api.get_project(project_name, sid):
            # get json responce
            json = res.json()
            logger.info(f"Success to access: {project_name}")
            logger.info("------------------------")
            logger.debug(json)
            logger.info("------------------------")
            # init and return ScrapBoxProject object
            return cls.from_json(json, sid)

        else:
            # Failed to access scrapbox project
            return None

    def fetch_pages(self, sid: str = "") -> bool:
        """
        Fetch all pages info from API
        """
        while 1:
            if self.pageCount > -1 and self.pageCount <= len(self.pages):
                logger.info(f"Completed to fetch all pages: {len(self.pages)} pages")
                return True

            skip_index = len(self.pages)
            fetch_limit = 1000
            logger.info(
                f"Start to fetch pages info: {skip_index} ~ {skip_index + fetch_limit - 1}"
            )
            if self.__fetch_pages_internal(sid=sid, limit=fetch_limit, skip=skip_index):
                logger.info(
                    f"Success to fetch pages: {skip_index} ~ {len(self.pages) - 1}"
                )
            else:
                logger.warning("Failed to fetch pages from API")
                return False

    def __fetch_pages_internal(self, sid="", limit=1000, skip=0):
        """"""
        if res := api.get_pages(self.name, sid=sid, limit=limit, skip=skip):
            # get json responce
            json = res.json()

            # update project info
            pagecount = json.get("count", 0)
            if self.pageCount != pagecount:
                self.pageCount = pagecount
                logger.debug(f"Update project page counts: {pagecount}")

            # init ScrapBoxPage objects
            json_pages = json.get("pages", list)
            for json_page in json_pages:
                page = ScrapBoxPage.from_json(json_page, self.name)
                self.pages.append(page)

            return True

        else:
            # Failed to access scrapbox project
            # raise
            return False
