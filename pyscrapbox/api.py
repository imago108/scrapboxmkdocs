import requests
from requests.models import Response


def get_project(project_name: str, sid: str = "") -> Response:
    BASE_URL = "https://scrapbox.io/api/projects/"
    url = f"{BASE_URL}{project_name}"
    cookies = {"connect.sid": sid}
    return requests.get(url, cookies=cookies)


def get_pages(project_name: str, sid: str = "", limit: int = 1000, skip: int = 0) -> Response:
    BASE_URL = "https://scrapbox.io/api/pages/"
    LIMIT = min(limit, 1000)
    SKIP = max(skip, 0)
    SORT = "created"

    url = f"{BASE_URL}{project_name}"
    payload = {
        "limit": LIMIT,
        "skip": SKIP,
        "sort": SORT,
    }
    cookies = {"connect.sid": sid}
    return requests.get(url, params=payload, cookies=cookies)


def get_page_text(project_name, page_title, sid=""):
    BASE_URL = "https://scrapbox.io/api/pages/"
    url = f"{BASE_URL}{project_name}/{page_title}/text"
    cookies = {"connect.sid": sid}
    return requests.get(url, cookies=cookies)
