from pyscrapbox import ScrapBoxProject
import re
import os

from pyscrapbox_markdown.markdown import MkDocsPage
from pyscrapbox_markdown import nav

from datetime import timezone, timedelta


from logging import getLogger, Formatter, StreamHandler, DEBUG

logger = getLogger(__name__)
fmt = Formatter("%(asctime)s - %(filename)s::%(funcName)s[%(levelname)s] %(message)s")
sh = StreamHandler()
sh.setLevel(DEBUG)
sh.setFormatter(fmt)
logger.setLevel(DEBUG)
logger.addHandler(sh)


def main():
    # ===========================
    # Settings - Publishing Page
    # ===========================
    # Ignore Pinned page if set True (default: True1)
    IS_IGNORE_PINNED_PAGE = True
    # Regular expression flagged publish page in description
    PUBLISH_TAG = "#publish"

    # ===========================
    # Settings - MkDocs
    # ===========================
    BUILD_DIR = "docs/"

    # Load ScrapBox settings from environment values
    SCRAPBOX_PROJECT = os.getenv("SCRAPBOX_PROJECT")
    SCRAPBOX_SECRET = os.getenv("SCRAPBOX_SECRET")

    if not SCRAPBOX_PROJECT:
        # load from dotenv
        from dotenv import load_dotenv

        filepath_env = os.path.join(os.path.dirname(__file__), ".env")
        logger.info(f"Try to load environments from {filepath_env}")
        load_dotenv(filepath_env)

        SCRAPBOX_PROJECT = os.getenv("SCRAPBOX_PROJECT")
        SCRAPBOX_SECRET = os.getenv("SCRAPBOX_SECRET")

        if not SCRAPBOX_PROJECT:
            logger.error(
                "Missing to scrap box project. Please set environmets value: $SCRAPBOX_PROJECT"
            )
            return

    sb_project = ScrapBoxProject.fetch_project(SCRAPBOX_PROJECT, SCRAPBOX_SECRET)
    if not sb_project:
        logger.error("Failed to access scrapbox project")
        return
    else:
        logger.info("Success to access to scrapbox project")

    if sb_project.fetch_pages(SCRAPBOX_SECRET):
        logger.info("Success to getch scrapbox pages")
    else:
        logger.error("Failed to fetch scrapbox pages")
        return

    # make compiled regex pattern
    pattern_publish_tag = f"(.*){PUBLISH_TAG}(.*)"
    prog = re.compile(pattern_publish_tag, re.IGNORECASE)

    # get published scrapbox page list
    pub_page_list = []
    for sb_page in sb_project.pages:
        # check pinned page
        if IS_IGNORE_PINNED_PAGE and sb_page.is_pinned():
            continue

        # check publish flag exists in description
        for desc in sb_page.descriptions:
            if prog.match(desc):
                # append publish page list
                pub_page_list.append(sb_page)
                break

    if len(pub_page_list) == 0:
        logger.error("Missing to publish pages")
        return

    logger.info(f"Founded {len(pub_page_list)} pages with tagged publish")

    # convert scrapbox to markdown
    mkpages_list = []
    for sb_page in pub_page_list:
        if sb_page.fetch_text(sid=SCRAPBOX_SECRET):
            logger.info(f"Start to convert page to markdown: {sb_page.title}")

            # convert scrapbox-page to markdown-page
            mkpage = MkDocsPage.from_scrapbox(sb_page, pattern_publish_tag)
            logger.info(f"Completed!: {sb_page.title}")

            # get export directory
            internal_url = mkpage.id
            mkpage.url = internal_url
            filepath = os.path.join(BUILD_DIR, f"{internal_url}.md")

            # exort markdown file
            with open(filepath, mode="w") as f:
                f.write(mkpage.text)

            logger.info(f"Exporeted: {filepath}")
            mkpages_list.append(mkpage)

        else:
            logger.warning(f"Failed to fetch page text: {sb_page.title}")
            continue

    # generate home page
    # sort by created
    filepath_home = os.path.join("templates/home.md")
    text_home = ""
    with open(filepath_home, mode="r") as f:
        text_home = f.read()

    if "{{ARTICLES}}" in text_home:
        result = ""
        TZ = timezone(timedelta(hours=+9), "JST")
        mkpages_list_sort_by_created = sorted(
            mkpages_list, key=lambda s: s.created, reverse=True
        )
        SETTINGS_MAX_ARTICLS = 10
        for mkpages in mkpages_list_sort_by_created[:SETTINGS_MAX_ARTICLS]:
            result += '<div class="kr-article">'
            # begin category
            result += '<a class="kr-article-category">'
            if len(mkpages.categoryBranch) > 0:
                category = mkpages.categoryBranch[0]
                if category == "USD":
                    result += f'<img class="emoji" draggable="false" alt={category} src="images/logo_usd_256.png" loading="lazy">'
                elif category == "UE4":
                    result += f'<img class="emoji" draggable="false" alt={category} src="images/logo_ue4_256.png" loading="lazy">'
                elif category == "Houdini":
                    result += f'<img class="emoji" draggable="false" alt={category} src="images/logo_houdini_256.png" loading="lazy">'
                elif category == "Davinci Resolve":
                    result += f'<img class="emoji" draggable="false" alt={category} src="images/logo_resolve_128.png" loading="lazy">'
                else:
                    result += f'{mkpages.categoryBranch[0]}'
            else:
                result += '/'
            result += '</a>'
            # end category
            # begin article content
            result += '<div class="kr-article-content">'
            # begin article title
            result += f'<a href="{mkpages.url}">'
            result += "<h3>"
            result += f"{mkpages.title}"
            result += "</h3>"
            result += "</a>"
            # end article title
            # begin article date
            result += '<div class="kr-date">'
            result += '<span class="kr-created">'
            result += mkpages.created.astimezone(TZ).strftime("%Y/%m/%d")
            result += "</span>"
            if mkpages.updated - mkpages.created > timedelta(days=+1):
                result += '<span class="kr-updated">'
                result += mkpages.updated.astimezone(TZ).strftime("%Y/%m/%d")
                result += "</span>"
            result += "</div>"
            # end article date
            result += "</div>"
            # end article content
            result += "</div>"
        filepath = os.path.join(BUILD_DIR, "index.md")
        result_text = text_home.replace("{{ARTICLES}}", result)
        with open(filepath, mode="w") as f:
            f.write(result_text)


    # generate nav map
    nav_text_list = nav.generate_nav(mkpages_list)
    with open("mkdocs.yml", "a") as f:
        for nav_text in nav_text_list:
        # update mkdocs.yml
            f.write("\n")
            f.write(nav_text)


if __name__ == "__main__":
    # execute only if run as a script
    main()
