
# Articles

{{ARTICLES}}

## Profile

<div style="display: flex;">
<div style="border-radius: 2.5em; min-width:5em; height:5em; background: var(--md-default-fg-color--lightest); margin-right: 1em;">
</div>
<div>
<p style="margin-top:0.1em; margin-bottom:0.1em">
    <i class="fa fa-user" aria-hidden="true" style="color: var(--md-default-fg-color--light); width: 1rem"></i>
    <span>Minami Tomonobu</span>
    <span aria-hidden="true" style="color: var(--md-default-fg-color--light);">/</span>
    <span>@imago108</span>
</p>
<p style="margin-top:0.1em; margin-bottom:0.1em">
    <i class="fa fa-envelope" aria-hidden="true" style="color: var(--md-default-fg-color--light); margin-right:0.5em;"></i>
    <a href="mailto:miyahuji111@gmail.com">miyahuji111@gmail.com</a>
</p>
<p style="margin-top:0.1em; margin-bottom:0.1em">
    <i class="fa fa-tags" aria-hidden="true" style="color: var(--md-default-fg-color--light); margin-right:0.5em;"></i>
    <span style="background-color: var(--md-default-fg-color--lightest);">Arnold</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Blender</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Houdini</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Katana</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Modo</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Nuke</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">OCIO</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">UE4</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">Unity</span>
    <span style="background-color: var(--md-default-fg-color--lightest);">USD</span>
</p>
</div>
</div>
